import React, {Fragment, useState, useEffect } from 'react';
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import axios from "axios";
const FormAPI = () => {
     const [texto, setTexto] = useState(""),
       [btn, setBtn] = useState(""),
       [imprimir, setImprimir] = useState("");

     // const textoValor = useSelector( state.texto );

     useEffect(() => {
       if (texto) {
         axios
           .get(`http://localhost:3002/api/v1/${texto}`)
           .then(response => setImprimir(response.data.texto)) // set cada entrada al estado
           .catch(error => console.log(error));
       }
     }, [btn]);
    return(
        <Fragment>
            <Form>  
                <FormGroup>
                <Label for="texto">Escribe lo que sea</Label>
                <Input onChange={ (event) => setTexto(event.target.value) }
                    type="text"
                    name="texto"
                    id="texto"
                    placeholder="Escribe lo que sea"
                    value={texto}
                    />
                </FormGroup>
                <Button onClick={ () => setBtn(texto) }>Enviar</Button>
            </Form>
            <div className="mt-4" id="rspAPI">{imprimir}</div>
        </Fragment>
    )
}
export default FormAPI;