import React  from "react"
import "bootstrap/dist/css/bootstrap.min.css";

import {useSelector} from "react-redux";
import {
  Container,
  Row,
  Col
} from "reactstrap";
import FormAPI from "./componentes/FormAPI";

const App = () => {
 
  return (
    <Container>
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <FormAPI />
        </Col>
      </Row>
    </Container>
  );
};

export default App;
