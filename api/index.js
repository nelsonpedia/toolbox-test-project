const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/api/v1/:parametro', (req, res) => {
    const texto = req.params.parametro;

    if(texto)
    {
        res.json({
            texto: texto
        });
        return
    }
    res.status(404).json({ message: "Parametro no encontrado" });

});

app.listen(3000, () => {
    console.log("El servidor está inicializado en el puerto 3000");
});

module.exports = app;