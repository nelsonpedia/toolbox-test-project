# Proyecto de Prueba ToolBox


## Paso 1: Desplegar los paquetes de instalacion
### Instalar Paquetes en el contenedor `api`
```
docker-compose run api npm install
```
### Instalar Paquetes en el contenedor `frontend`
```
docker-compose run frontend yarn install
```

## Paso 2: Cerrar la ejecución de los contenedores 
```
docker-compose down
```
**NOTA:** el paso dos es necesario para limpiar la ejecución de los contenedores
##

## Paso 3: Iniciar Proyecto
```
docker-compose up -d 
```

## Paso 4: se debe ir al navegador y colocar las siguientes url:  `http://localhost:3004/` (**frontend**) y  `http://localhost:3002/` (**api**) 

## Paso 5: la prueba unitaria se ejecuta en API de la siguiente manera
```
docker-compose run api npm run test
```